<?php

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Response;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;

$loader = new Loader();

$loader->registerDirs(
	[
		'app/models',
	]
);

$loader->register();

$view = new View();
$view->setViewsDir('app/views/');
$view->render('cards','index');
$view->finish();

$di = new FactoryDefault();

$di->set(
	'db',
	function(){
		return new PdoMysql(
			[
				'host'=>'localhost',
				'username'=>'root',
				'password'=>'',
				//'password'    => 'Dy945sX55sh54N2j',
				'dbname'=>'alonso',
				'charset' => 'utf8'
			]
		);
	}
);

$app = new Micro($di);

$app->get(	
	'/api/cards',
	function() use ($app) {
		$phql = 'SELECT * FROM Tarjetas';

		$cards = $app->modelsManager->executeQuery($phql);

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
				'travel' => $card->travel,
				'low' => $card->low,
				'armor' => $card->armor,
				'rewards' => $card->rewards,
				'commision' => $card->commision,
				'utm' => $card->utm,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/single/{slug}',
	function($id) use ($app) {
		$phql = 'SELECT * FROM Tarjetas WHERE slug = :slug:';

		$singlecard = $app->modelsManager->executeQuery(
			$phql,
			[
				'slug' => $id,
			]
		)->getFirst();

		$response = new Response();

		if($singlecard === false){
			$response->setJsonContent(
				[
					'status' => 'NOT-FOUND'
				]
			);
		} else {
			$response->setJsonContent(
				[
					'id' => $singlecard->id,
					'slug' => $singlecard->slug,
					'name' => $singlecard->name,
					'tagline' => $singlecard->tagline,
					'description' => $singlecard->description,
					'type' => $singlecard->type,
					'cat' => $singlecard->cat,
					'catpon' => $singlecard->cat_pon,
					'datecalc' => $singlecard->date_calc,
					'annualcomm' => $singlecard->annual_comm,
					'img' => $singlecard->img,
					'travel' => $singlecard->travel,
					'low' => $singlecard->low,
					'armor' => $singlecard->armor,
					'rewards' => $singlecard->rewards,
					'commision' => $singlecard->commision,
					'utm' => $singlecard->utm,
				],JSON_UNESCAPED_UNICODE
			);
		}

		return $response;
	}
);

$app->get(
	'/api/cards/travel',
	function() use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE travel = true');

		$cards = $query->execute();

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/low',
	function() use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE low = true');

		$cards = $query->execute();

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/armor',
	function() use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE armor = true');

		$cards = $query->execute();

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/rewards',
	function() use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE rewards = true');

		$cards = $query->execute();

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/commision',
	function() use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE commision = true');

		$cards = $query->execute();

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'slug' => $card->slug,
				'name' => $card->name,
				'tagline' => $card->tagline,
				'description' => $card->description,
				'type' => $card->type,
				'cat' => $card->cat,
				'catpon' => $card->cat_pon,
				'datecalc' => $card->date_calc,
				'annualcomm' => $card->annual_comm,
				'img' => $card->img,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->get(
	'/api/cards/type/{type}',
	function($type) use ($app) {
		$query = $app->modelsManager->createQuery('SELECT * FROM Tarjetas WHERE type = :type:');

		$cards = $query->execute(
			[
				'type' => $type,
			]
		);

		$data = [];

		foreach ($cards as $card){
			$data[] = [	
				'id' => $card->id,
				'name' => $card->name,
				'slug' => $card->slug,
			];
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

	}
);

$app->handle();